# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#client information

module SopranInfo
	CLIENTNAME = "Sopran Audio Player"
	CLIENTNAMESHORT = "Sopran"
	CLIENTDIRECTORYNAME = "sopran"

	#verion number
	CLIENTVERSION = "v0.2"
	
	CLIENTWEB = "http://www.digital-d.at"
	CLIENTDESCRIPTION = "A simple Ruby client for XMMS2"
	CLIENTCONTRIBUTORS = "Marc Menghin <marc.menghin@gmail.com>"
end
