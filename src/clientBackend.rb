# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Sopran
  class ClientBackend
    #config constants
    CONFIG_CATEGORY_XMMS = "XMMS2"
    CONFIG_XMMS_AUTOSTARTDAEMON = "xmms2_autostartdeamon"
    CONFIG_XMMS_AUTOSTARTPLAYBACK = "xmms2_autostartplayback"
    CONFIG_XMMS_AUTOEXITONCLIENTEXIT = "xmms2_autoexitonclientexit"
    CONFIG_XMMS_AUTOSTOPPLAYBACK = "xmms2_autoexitonclientexit"

    @@logger = nil
    def initialize()
      if (@@logger == nil)
        @@logger = Log4r::Logger.new(self.class().name)
      end

      @connected = false;
      @songChanged = Hash.new
      @songChangedCount = 0
      @historyMax = 100
    end

    def connectSave(autostart)
      if ((not self.connect()) && autostart)
        @@logger.info "XMMS2 daemon not found, starting it now."
        if (not system "xmms2-launcher")
          @@logger.error "an error occured launching the XMMS2 daemon"
          @@logger.fatal $? #output full error
        else
          sleep 2 #sleep for short time and let xmms2d start up (2 sec)
        end
        if (not self.connect())
          @logger.error "Error connecting to XMMS2 daemon."
          self.printLastError
        end
      end
    end

    def connect
      if (@connected)
        disconnect
      end
      begin
        require 'xmmsclient'
        @@logger.info "XMMS2 Version: #{Xmms::VERSION}"

        @client = Xmms::Client.new(Sopran::CLIENTNAMESHORT)
        path = ENV['XMMS_PATH']
        if (path == nil)
          @client.connect()
        else
          @client.connect(path)
        end
        @client.on_disconnect { disconnect }
        @client.broadcast_quit.notifier { |res| disconnect }
        @client.broadcast_playback_current_id.notifier { |result|
          newid = result
          fireSignalSongChanged(newid)
        }
        @connected = true;
      rescue Xmms::Client::ClientError
        @@logger.error 'Failed to connect to XMMS2 daemon.'
        @@logger.error 'XMMS2d is not running or using the wrong IPC path.'
        @@logger.error $? #output full error
        @connected = false;
      end

      return @connected
    end

    def connectIfNotAlready
      value = false
      if (not connected?)
        value = connect
      else
        value = true
      end
      return value
    end

    def connected?
      return @connected
    end

    def disconnect
      if (@cleint != nil)
        @client.delete!
        @client = nil
      end
      @connected = false;
      @songHistory.clear()

      #update song displays
      fireSignalSongChanged(0)
    end

    def play
      if (not connectIfNotAlready)
        return
      end
      @client.playback_start.wait
    end

    def stop
      if (not connectIfNotAlready)
        return
      end
      @client.playback_stop.wait
    end

    def pause
      if (not connectIfNotAlready)
        return
      end
      @client.playback_pause.wait
    end

    def playing?
      if (not connectIfNotAlready)
        return false
      end
      
      stat = @client.playback_status.wait
      
      Util::DebugHelper.printInfo(stat)
      
      if (@client.playback_status.wait.value == Xmms::Client::PLAY)
        return true
      else
        return false
      end
    end

    def playPause
      if (not connectIfNotAlready)
        return
      end
      if (self.playing?)
        self.pause
      else
        self.play
      end
    end

    def nextTrack
      if (not connectIfNotAlready)
        return
      end
      @client.playback_tickle.wait
    end

    def previousTrack
      if (not connectIfNotAlready)
        return
      end

      @@logger.warn "Previous Track not supported at the moment."
      self.play
    end

    def getInfo(id)
      if (not connectIfNotAlready)
        return
      end

      require 'lib/debugHelper'
      
      
      
      return @client.medialib_get_info(id).wait.value
    end

    def getCurrentlyPlayingId
      if (not connectIfNotAlready)
        return
      end

      return @client.playback_current_id.wait.value
    end

    def signalRegisterSongChanged(&block)
      @songChanged[@songChangedCount] = block
      @songChangedCount = @songChangedCount + 1
    end

    def signalUnregisterSongChanged(number)
      if (@songChanged.has_key?(number))
        @songChanged.delete(number)
      else
        @@logger.warn "No Event with ID #{number} registered."
      end
    end

    def fireSignalSongChanged(newId)
      @songChanged.each_value {|value|
        value.call(self, newId)
      }
    end

    def printStats
      if (not connectIfNotAlready)
        return
      end
      result = @client.main_stats.wait.value
      @@logger.info "Server Info"
      result.each { |key, value|
        @@logger.info " » #{key} => #{value}"
      }
    end

    def printPlaylists
      if (not connectIfNotAlready)
        return
      end
      result = @client.playlist_list.wait.value.to_a
      @@logger.info "Playlists:"
      result.each { |value|
        @@logger.info " » #{value}"
      }
    end

    def printLastError
      if (not connectIfNotAlready)
        return
      end
      error = @client.last_error
      if (error != nil)
        @@logger.error "Last Error in XMMS2 Clientlib:"
        @@logger.error error
      end
    end

    def exitXMMS2Deamon
      if (not connectIfNotAlready)
        return
      end

      @client.quit
      self.printLastError
      @client = nil
      @connected = false
    end
  end
end