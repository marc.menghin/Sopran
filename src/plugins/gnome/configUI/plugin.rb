#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'plugins/basicPlugin'

module Sopran::Plugins::ConfigUI
	class Plugin < Sopran::Plugins::BasicPlugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "ConfigUI"
			@version = "v0.2"
			@description = "A GTK+ based UI to configure Sopran and its plugins."
			@id = :pluginGnomeConfigUI
			@configFile = File.join(Sopran::CLIENTCONFIGDIRECTORY, "config.xml")
			@gladeFile =  File.join(PLUGIN_PATH, "configUI.glade")
			@glade = nil
			@configUIs = Hash.new
		end
		
		def startup (pluginContext)
			if (not pluginContext.pluginManager.exists?(:pluginConfigLoaderSaver))
				#load config if not already done
				saverloader = pluginManager[:pluginConfigLoaderSaver]
				saverloader.load(@configFile, configManager)
				return false
			end 
			return true
		end

		def shutdown (pluginContext)
			if (@glade != nil)
				self.hide
			end
			@configLoaded = false
		end
		
		def show(pluginManager, configManager)
			if (pluginManager.exists_running?(:pluginConfigLoaderSaver) && @glade == nil)
				@configUIs.each { |key, frame|
					if (frame.methods.include?(:load) && frame.methods[:load].arity == 1)
						frame.load(configManager)
					end
				}
											
				#show options ui
				@glade = GladeXML.new(@gladeFile) {|handler| method(handler)}
				@optionsWindow = @glade.get_widget("optionsWindow")
				@optionsWindow.signal_connect("destroy") { |w| hide }
								
				@optionsWindowOK = @glade.get_widget("okButton")
				@optionsWindowCancel = @glade.get_widget("cancelButton")
				
				@optionsWindowOK.signal_connect("clicked") { |w| 
					save(pluginManager, configManager)
					hide }
				@optionsWindowCancel.signal_connect("clicked") { |w| 
					hide }

				@optionsWindow.show
			else
				puts "ERROR: plugin loader saver not found."
			end
		end
		
		def save(pluginManager, configManager)
			
			@configUIs.each { |key, frame|
				if (frame.methods.include?(:save) && frame.methods[:save].arity == 1)
					frame.save(configManager)
				end
			}
			
			if (pluginManager.exists_running?(:pluginConfigLoaderSaver))
				saverloader = pluginManager[:pluginConfigLoaderSaver]
				saverloader.save(@configFile, configManager)
			else
				puts "ERROR: plugin loader saver not found."
			end
		end
		
		def hide()
			if (@glade != nil)
				@optionsWindow.hide
				@optionsWindow = nil
				@glade = nil
			end
		end
		
		def addConfigUI(key, frame)
			@configUIs[key] = frame
		end
	end	
end
