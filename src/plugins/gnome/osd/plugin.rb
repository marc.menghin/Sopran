#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'plugins/basicPlugin'

module Sopran::Plugins::OSD
class Plugin < Sopran::Plugins::BasicPlugin
	PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
	
	def initialize
		super()
		@name = "OSD"
		@version = "v0.1"
		@description = "On Screen Display of the currently playing Song."
		@id = :pluginGnomeOSD
	end
	
	def startup (pluginContext)
		return true
	end
	
	def shutdown (pluginContext)
	end		
	
end

end
