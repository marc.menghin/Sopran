#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'libglade2'
require 'sopran'

class MainwindowGlade
  def initialize(path)
    @glade = GladeXML.new(path) {|handler| method(handler)}
    @mainWindow = @glade.get_widget("mainWindow")
		@mainAboutButton = @glade.get_widget("mainAboutButton")
		
		@aboutWindow = @glade.get_widget("aboutWindow")
		@aboutClose = @glade.get_widget("aboutClose")
		
		@mainWindow.set_title($CLIENTNAME + " - " + $CLIENTVERSION)
		@mainWindow.signal_connect("destroy") { |w| on_close }
		@mainAboutButton.signal_connect("clicked") { |w| on_about_show }
		
		@aboutClose.signal_connect("clicked") { |w| on_about_close }
  end

	def on_close
		Gtk.main_quit
	end
	
	def on_about_close
		@aboutWindow.hide
	end
	
	def on_about_show
		@aboutWindow.show
	end
	
	def show
		@mainWindow.show
	end
end
