#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Sopran::Plugins::SimplePlayer
	
class Plugin < Sopran::Plugins::BasicPlugin
	PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
	
	def initialize
		super()
		@name = "Simple Player"
		@version = "v0.1"
		@description = "A Simple WinAMP like player interface."
		@id = :pluginGnomeSimplePlayer
	end
	def startup (pluginContext)
		return true
	end
	
	def shutdown (pluginContext)
	end		
	
end

end
