#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Sopran::Plugins::TrayIcon
  class TrayIcon
    attr_reader :icon, :iconImg
    def initialize(xmms2Client, pluginContext)
      require 'gtk2'

      basepath = File.dirname(__FILE__) + File::SEPARATOR
      file = basepath + "trayIcon.svg"
      gladepath = basepath + "trayIcon.glade"

      @builder = Gtk::Builder.new
      @builder << gladepath

      @menu = @builder["menuTrayIcon"]
      @menuItemQuit = @builder["menuitem_Quit"]
      @menuItemPlay = @builder["trayMenu_Play"]
      @menuItemPrevious = @builder["trayMenu_Previous"]
      @menuItemPrevious.visible = false

      @menuItemNext = @builder["trayMenu_Next"]
      @menuItemAbout = @builder["trayMenu_About"]
      @menuItemPreferences = @builder["trayMenu_Preferences"]
      @menuItemPreferences.visible = false;

      @iconImg = Gdk::Pixbuf.new(file)
      @icon = Gtk::StatusIcon.new()
      @icon.stock = Gtk::Stock::REFRESH
      @icon.tooltip = "loading system ...";

      @icon.signal_connect('popup-menu') { |w, button, time|
        if (xmms2Client.playing?)
          @menuItemPlay.active = true
        else
          @menuItemPlay.active = false
        end

        if (xmms2Client.connected?)
          @menuItemPlay.sensitive = true
          @menuItemPrevious.sensitive = true
          @menuItemNext.sensitive = true
        else
          @menuItemPrevious.sensitive = false
          @menuItemNext.sensitive = false
          @menuItemPlay.sensitive = false
        end
        @menu.popup(nil, nil, button, time)

      }

      @icon.visible = false
    end

    def signalPreferences
      return @menuItemPreferences.signal_connect("activate")
    end

    def signalQuit
      return @menuItemQuit.signal_connect("activate")
    end

    def signalPlay
      return @menuItemPlay.signal_connect("activate")
    end

    def signalPrevious
      return @menuItemPrevious.signal_connect("activate")
    end

    def signalNext
      return @menuItemNext.signal_connect("activate")
    end

    def signalIconClick
      return @icon.signal_connect('activate')
    end

    def signalAbout
      return @menuItemAbout.signal_connect("activate")
    end

    def tooltip=(value)
      @icon.tooltip = value
    end

    def show
      @icon.pixbuf = @iconImg
      @icon.visible = true
    end

    def hide
      @icon.visible = false
    end
  end
end
