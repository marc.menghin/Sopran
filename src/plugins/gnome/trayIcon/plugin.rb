#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicMainPlugin'

module Sopran::Plugins::TrayIcon
  class Plugin < Sopran::Plugins::BasicMainPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
    def initialize
      super()
      @name = "TrayIcon"
      @version = "v0.2"
      @description = "A TrayIcon to control basic XMMS2 commands."
      @id = :pluginGnomeTrayIcon

      @icon = nil
      @eventClick = Hash.new
      @currentlyPlaying = nil
      @gnomeProgramm = nil
    end

    def startup (pluginContext)
      #load requires at startup
      require 'gtk2'
      require 'gnome2'
      require 'gnome/trayIcon/trayIcon'
      require 'lib/songInfo'

      if defined? Gnome then
        @gnomeProgramm = Gnome::Program.new(Sopran::CLIENTNAME, Sopran::CLIENTVERSION)
      end

      if @icon != nil
        shutdown(pluginContext)
      end

      if (not checkForDependencies(pluginContext))
        @@logger.info "Unmeet dependencies for TrayIcon Plugin"
        return false
      end

      @icon = TrayIcon.new(pluginContext.xmms2Client, pluginContext)
      @icon.show

      @icon.signalPreferences { |x|
        if (pluginContext.pluginManager != nil)
          plugin = pluginContext.pluginManager[:pluginConfigUI]
          if (plugin != nil)
            plugin.show(pluginContext.pluginManager, pluginContext.configManager)
          else
            @@logger.error "ConfigUI plugin not found."
          end
        else
          @@logger.error "PluginManager is nil."
        end
      }

      @icon.signalNext {|x|
        pluginContext.xmms2Client.nextTrack
      }

      @icon.signalPlay {|x|
        pluginContext.xmms2Client.playPause
      }

      @icon.signalQuit {|x|
        pluginContext.exitPlatform
      }

      @icon.signalPrevious {|x|
        pluginContext.xmms2Client.previousTrack
      }

      @icon.signalIconClick {|x|
        #pluginContext.reloadAllPlugins
      }

      @icon.signalAbout {|x|
        if (pluginContext.pluginManager != nil)
          plugin = pluginContext.pluginManager[:pluginAboutBox]
          if (plugin != nil)
            plugin.show
          else
            @@logger.error "About plugin not found."
          end
        else
          @@logger.error "PluginManager is nil."
        end
      }

      @icon.tooltip = Sopran::CLIENTNAME + " " + Sopran::CLIENTVERSION + " (XMMS2 Client)"

      @signalSongChangeID = pluginContext.xmms2Client.signalRegisterSongChanged { |xmms2Client, newSongID |
        @@logger.info "Song changed to: #{newSongID.to_s}"
        updateSong(xmms2Client, newSongID)
      }

      return true
    end

    def checkForDependencies(pluginContext)
      if (not pluginContext.pluginManager.exists?(:pluginGnomeAboutBox))
        return false
      end
      if (not pluginContext.pluginManager.exists?(:pluginGnomeConfigUI))
        return false
      end
      return true
    end

    def updateSong(xmms2Client, newSongID)
      currentlyPlaying = nil
      if (newSongID != 0)
        currentlyPlaying = Sopran::Lib::SongInfo.new(xmms2Client.getInfo(newSongID))
      else
        currentlyPlaying = Sopran::Lib::SongInfo.new
      end
      if ($DS)
        puts currentlyPlaying.format("Now Playing: %artist% - %title%")
      end
      @icon.tooltip = currentlyPlaying.format("%track%. %title%\nfrom %album%\nby %artist%")
    end

    def shutdown (pluginContext)
      pluginContext.xmms2Client.signalUnregisterSongChanged(@signalSongChangeID)
      @icon.hide
      @icon = nil
    end

    def registerEventClick(&block)
      zahl = @eventClick.length
      @eventClick[zahl] = block
      return zahl
    end

    def unregisterEventClick(number)
      if (@eventClick.has_key?(number))
        @eventClick.delete(number)
      else
        @@logger.info "not correct number to deregister event"
      end
    end

    def mainLoop
      if defined? Gtk then
        Gtk.main
      end

    end

    def exitProgramm
      if defined? Gtk then
        Gtk.main_quit
      end
    end

  end
end
