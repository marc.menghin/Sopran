#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'gtk2'
require 'gnome2'

#module Sopran::Plugins::MultimediaKeys
	class MultimediaKeys
		@@KEY_NONE = 0
		@@KEY_PLAY = 1
		@@KEY_PLAYPAUSE = 2
		@@KEY_STOP = 3
		@@KEY_PAUSE = 4
		@@KEY_NEXT = 5
		@@KEY_PREVIOUS = 6
		@@KEY_VOLUMEUP = 7
		@@KEY_VOLUMEDOWN = 8

		def initialize
			@displays = Hash.new
		end
		
		def hookIntoSystem
			
			#unhook old events first
			self.unhookFromSystem

			displayManager = Gdk::DisplayManager.get
			displays = displayManager.displays
			displays.each { |display|
				screenMap = Hash.new
				for i in 0...display.n_screens
					screen = display.get_screen(i)
					window = screen.root_window #used to catch events on that screen
					signal_id = window.signal_connect(Gdk::Event::KEY_PRESS) { |w, eventKey|
						handleEvent(w, eventKey)
					}
					screenMap[i] = signal_id
				end
				@displays[display.name] = screenMap
			}
		end

		def handleEvent(w, eventKey)
			puts("global signal: " + w.to_s + " - " + key.to_s)
		end
		
		def unhookFromSystem
			#removes all registered events
			
			displayManager = Gdk::DisplayManager.get
			displays = displayManager.displays
			displays.each { |display|
				screenMap = @displays[display.name]
				if (screenMap != nil)
					for i in 0...display.n_screens
						screen = display.get_screen(i)
						signal_id = screenMap[i]
						window = screen.root_window
						if window.signal_handler_is_connected?(signal_id)
							window.signal_handler_disconnect(signal_id)
						end
					end
					screenMap.clear
				end
			}
			@displays.clear
		end
	end
#end

multimedia = MultimediaKeys.new
multimedia.hookIntoSystem

Gtk.main

multimedia.unhookFromSystem

