# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Sopran::Plugins
  class BasicMainPlugin < Sopran::Plugins::BasicPlugin
    
    def mainLoop
      puts "Main not implemented in '#{self.class().name}'."
    end
    
    def exitProgramm
      puts "exit not implemented in '#{self.class().name}'."
    end
    
  end
end